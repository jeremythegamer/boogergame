/// <summary>
/// Class: 		Image import.
/// Author: 	Chris Dixon
/// PUrpose:	Allows the user to take a picture throw the game and display the picture.
/// </summary>
using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;

public class ImageImport : MonoBehaviour
{

	WebCamDevice[] _devices;
	WebCamTexture _webCam;
	string _deviceName;

	// Use this for initialization
	void Start ()
	{
		_webCam = null;
	}

	/// <summary>
	/// Gets the device.
	/// Called when camera is turned on.
	/// TODO: 1. Ask permission to use the device.
	/// 	  2. Allow the user to select the device they want. 
	/// 			Currently the last device found is used.
	/// </summary>
	public void GetDevice ()
	{
		if (_webCam == null)
		{
			_devices = WebCamTexture.devices;
			for (int i = 0; i < _devices.Length; i++)
			{
				_deviceName = _devices [i].name;
				Debug.Log ("Devices Available: " + _devices [i].name);
			}
			
			MeshRenderer renderer = GetComponent<MeshRenderer> ();
			
			_webCam = new WebCamTexture (_deviceName);
			renderer.material.mainTexture = _webCam;
//			Quaternion rotation = Quaternion.Euler(0, 90, 0);
//			renderer.material.SetMatrix("_rotation",  Matrix4x4.TRS (Vector3.up, rotation, new Vector3(1, 1, 1)));
			_webCam.Play ();
		}
		else
		{
			if (_webCam.isPlaying)
			{
				_webCam.Stop ();
			}
			else
			{
				_webCam.Play ();
			}
		}
	}

	/// <summary>
	/// Captures the picture.
	/// Called when the (Snap/Capture) button is clicked. 
	/// </summary>
	public void CapturePicture ()
	{
		if (_webCam.isPlaying)
		{
			_webCam.Pause ();
		}
		else
		{
			_webCam.Play ();
		}
	}

	/// <summary>
	/// Swap this instance.
	/// Swaps between front and back camera
	/// </summary>
	public void Swap ()
	{
		if (_webCam != null)
		{
			_webCam = null;

			if (_webCam.name.Contains ("Front"))
				_webCam = new WebCamTexture ("Remote Back Camera");
			else
				_webCam = new WebCamTexture ("Remote Front Camera");
			
			MeshRenderer renderer = GetComponent<MeshRenderer> ();
			renderer.material.mainTexture = _webCam;
			_webCam.Play ();
		}
	}


	
	// Update is called once per frame
	void Update ()
	{
	
	}


}
