﻿using UnityEngine;
using System.Collections;

enum GameState
{
    None = 0,
    Holding,
    Thrown,
    Hit
}

[RequireComponent (typeof(SpriteRenderer))]
public class ThrowObjectController : MonoBehaviour
{
    public GameObject target; // set in inspector
    public GameObject midLine; // set in inspector
    public Sprite splatSprite;
    GameState currentState = GameState.None;
    SpriteRenderer spriteRenderer;

    //curve on the Y
    //    float throwSpeed = 2.0f;
    //    float upSpeed = 1.0f;
    //    float gravity = 0.4f;
    float throwSpeed = 3.0f;
    float upSpeed = 0.75f;
    float gravity = 0.4f;
    // Use this for initialization
    void Start ()
    {
        spriteRenderer = GetComponent<SpriteRenderer> ();
    }
    
    // Update is called once per frame
    void Update ()
    {
        HandleInput ();

        HandleMovement ();
    }

    void HandleInput ()
    {
        if ((Input.GetMouseButton (0) && (currentState == GameState.None || currentState == GameState.Holding)) ||
            (Input.GetMouseButton (1) && (currentState == GameState.None || currentState == GameState.Holding)))
        {
            Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

            float x = ray.origin.x + ray.direction.x;
            float y = ray.origin.y + ray.direction.y;
            float z = ray.origin.z + ray.direction.z;

            if (y < midLine.transform.position.y)
            {
                if (currentState == GameState.None)
                    currentState = GameState.Holding;

                transform.position = new Vector3(x, y, z);
            }
            else if (currentState == GameState.Holding)
                currentState = GameState.Thrown;
        }
        else if (Input.GetMouseButtonUp(0) && currentState == GameState.Holding)
            currentState = GameState.None;
        else if (Input.GetMouseButtonUp (1) && currentState == GameState.Holding)
            currentState = GameState.Thrown;
    }

    void HandleMovement ()
    {
        switch (currentState)
        {
            default:
            case GameState.None:
                break;
            case GameState.Holding:
                if (transform.position.y > midLine.transform.position.y)
                {
                    currentState = GameState.Thrown;
                }
                break;
            case GameState.Thrown:
                transform.position = new Vector3 (transform.position.x, 
                                                 transform.position.y + upSpeed * Time.deltaTime, 
                                                 transform.position.z);
                Camera.main.transform.position = new Vector3 (Camera.main.transform.position.x, 
                                                             Camera.main.transform.position.y, 
                                                             Camera.main.transform.position.z + throwSpeed * Time.deltaTime);
                // falling
                upSpeed -= gravity * Time.deltaTime;
                if (transform.position.z >= target.transform.position.z)
                {
                    transform.position = new Vector3 (transform.position.x,
                                                     transform.position.y,
                                                     target.transform.position.z);

                    spriteRenderer.sprite = splatSprite;
                    currentState = GameState.Hit;
                }
                break;
        }
    }
}